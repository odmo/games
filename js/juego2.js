let turno = 1;
// Estados jugador 
let vida = 100;
let vivo = true;
let envenenado = false;
let quemado = false;


// jugadas CPU
let atacar = 0;
let quemar = 1;
let envenenar = 2;
let fallar = 3;

// objetos 
let pocion = 0;
let colaFenix = 1;
let pocionQuemaduras = 2;
let pocionAntidoto = 3;

const suma = (num1, num2) => { return parseInt(num1) + parseInt(num2) ; };

const muestraEstadoJugador =() =>{
    console.log("vida: " + vida)
    if(envenenado){
        console.log("Envenenado")
    }
    if(quemado){
        console.log("Quemado")
    }
}

const estasVivo =() =>{ return vida > 0}

const usarItem = (objeto) => {
    
    if(objeto === pocion) {
        if(estasVivo()){
            console.log(" estas vivo", estasVivo())
            vida += 50;
        }else{
            console.log("Estas muerto ! No se puede usar");
        }
    }

    if(objeto === colaFenix){
        if(!estasVivo()){
            vida=20;
        }else{
            console.log("Ya estas vivo! No se puede usar");
        }
        
    }

    if(objeto === pocionQuemaduras){
        if(quemado){
            quemado = false;
        }else{
            console.log("No estas quemado! No se puede usar");
        }
    }

    if(objeto === pocionAntidoto){
        if(envenenado){
            envenenado = false;
        }else{
            console.log("No estas envenenado! No se puede usar");
        }
    }

    muestraEstadoJugador();
}

const juegaTurno = () => {
    if(estasVivo()){
        
        const juegaCPU = Math.floor(Math.random() * 4); 
        if (juegaCPU === atacar){
            vida -= 10;
            console.log("He atacado al jugador")
        }
        if (juegaCPU === quemar){
            quemado = true;
            console.log("He quemado al jugador")
        }
        if (juegaCPU === envenenar){
            envenenado = true;
            console.log("He envenenado al jugador")
        }
        if (juegaCPU === fallar){
            console.log("He fallado miserablemente")
        }
        muestraEstadoJugador();
    }else {
        console.log("Game Over has muerto")
    }

    
} 


